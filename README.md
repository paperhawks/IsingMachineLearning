# Ising Model Machine Learning

Code for machine learning the Ising model by using a normal feed-forward neural network. The bulk of this project was done as a final project for physics 568 in Rutgers University. Collaborations to improve the code or porting it over to another system is welcome, feel free to contact me at conanhuang@physics.rutgers.edu.

# Code Overview

The code is composed of two parts.

1. Creation of the database: The database on which the model trains is created by a typical Markov chain Monte Carlo (MCMC) sampling of a random grid that represents the Ising model. This is done for multiple temperatures and multple grid sizes.

2. Training the model: The feed-forward neural network is trained using conjugate gradient descent in Matlab. Currently, one must specify which training set one wishes to use and must modify the code directly.

# Compiling and running the programs

1. Compiling the first piece of code should be available as long as `g++` exists. Simply type `make` in the same directory as the Makefile and `GenIsing` should appear to run via `./GenIsing`.

2. Training the neural network should need nothing more than MATLAB base and should just be running the script `ising_NN.m`. 
