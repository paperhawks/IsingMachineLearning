import tensorflow as tf
import tensorflowvisu
tf.set_random_seed(0)

def parse(text,N):
    systemVals = []
    lattices = []
    lattice = []
    lattCounter = 0

    for lineNum, line in enumerate(text):
        #grab lattice
        if (lineNum+1)%(N+1) != 0:
            lattice.append([float(i) for i in line.lstrip().rstrip().split()])

        #grab regression values
        else:
            systemVals.append([float(i) for i in line.lstrip().rstrip().split(',')])
            lattices.append(lattice)
            lattice = []

    return lattices, systemVals

def readData(T, N, path):
    """
    Reads data at temperature T and size N.
    """

    #regularize bad choice of formatting
    if T == 1:
        T = int(T)

    #create file name
    fileName = "T" + str(T) + ".N" + str(N)

    #concatonate with filename
    filePath = path + fileName

    #read data
    text_file = open(filePath, "r")
    lattices, systemVals = parse(text_file,N)
    text_file.close()

    return lattices, systemVals

def createBatch(lattices, systemVals, batchSize):
    """
    Creates batches for training and validation in accordance to batchSize.
    """
    images = []
    labels = []

    for i in range(batchSize):
        #create the "images" and "labels" for testing
        images.append(lattices[i])
        labels.append(systemVals[i][0])

        #delete them from the dataset so they cannot be used again
        del lattices[i]
        del systemVals[i]  

    return images, labels


# You can call this function in a loop to train the model, 100 images at a time
def training_step(i, update_test_data, update_train_data):

    # training on batches of 100 images with 100 labels
    data, labels = createBatch(lattices, systemVals, 100)
    trainingData, testData, trainingLabel, testLabel = splitData(data, labels)

    # compute training values for visualisation
    if update_train_data:
        a, c, im, w, b = sess.run([accuracy, cross_entropy, I, allweights, allbiases], feed_dict={X: trainingData, Y_: trainingLabel})
        datavis.append_training_curves_data(i, a, c)
        datavis.append_data_histograms(i, w, b)
        datavis.update_image1(im)
        print(str(i) + ": accuracy:" + str(a) + " loss: " + str(c))

    # compute test values for visualisation
    if update_test_data:
        a, c, im = sess.run([accuracy, cross_entropy, It], feed_dict={X: testData, Y_: testLabel})
        datavis.append_test_curves_data(i, a, c)
        datavis.update_image2(im)
        print(str(i) + ": ********* epoch " + str(i*100//mnist.train.images.shape[0]+1) + " ********* test accuracy:" + str(a) + " test loss: " + str(c))

    # the backpropagation training step
    sess.run(train_step, feed_dict={X: trainingData, Y_: trainingLabel})

def splitData(data, labels):
    """
    Splits the data into training and test sets.
    """
    trainingData = []
    trainingLabel = [] 
    testData = []
    testLabel = []
    for i in range(int(len(data)/10)):

        #partition data by copying
        testData.append(data[i])
        testLabel.append(labels[i])

        #delete to ensure partition (no overlap)
        del data[i]
        del labels[i]

    trainingData = data
    trainingLabel = labels

    return trainingData, testData, trainingLabel, testLabel


def main():
    # Physical parameters
    N = 10 # size of Ising model
    T = 1.0 #temperature
    batch_N = 10 # size of training batch

    #reads lattices and ising properties of lattice
    path = "./N" + str(N) + "/"
    lattices, systemVals = readData(T, N, path)
    trainingData, testData, trainingLabel, testLabel = splitData(lattices, systemVals)

    # input X: NxN Ising images, the first dimension (None) will index the images in the mini-batch
    X = tf.placeholder(tf.float32, [None, N, N, 1])
    # correct answers will go here
    Y_ = tf.placeholder(tf.float32, [None, batch_N])
    # weights W[784, 10]   784=28*28
    W = tf.Variable(tf.zeros([N*N, batch_N]))
    # biases b[10]
    b = tf.Variable(tf.zeros([batch_N]))

    # flatten the images into a single line of pixels
    # -1 in the shape definition means "the only possible dimension that will preserve the number of elements"
    XX = tf.reshape(X, [-1, N*N])

    # The model
    Y = tf.nn.softmax(tf.matmul(XX, W) + b)

    # loss function: cross-entropy = - sum( Y_i * log(Yi) )
    #                           Y: the computed output vector
    #                           Y_: the desired output vector

    # cross-entropy
    # log takes the log of each element, * multiplies the tensors element by element
    # reduce_mean will add all the components in the tensor
    # so here we end up with the total cross-entropy for all images in the batch
    cross_entropy = -tf.reduce_mean(Y_ * tf.log(Y)) * 1000.0  # normalized for batches of 100 images,
                                                              # *10 because  "mean" included an unwanted division by 10

    # accuracy of the trained model, between 0 (worst) and 1 (best)
    correct_prediction = tf.equal(tf.argmax(Y, 1), tf.argmax(Y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    # training, learning rate = 0.005
    train_step = tf.train.GradientDescentOptimizer(0.005).minimize(cross_entropy)

    # init
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)

    datavis.animate(training_step, iterations=2000+1, train_data_update_freq=10, test_data_update_freq=50, more_tests_at_start=True)

    # to save the animation as a movie, add save_movie=True as an argument to datavis.animate
    # to disable the visualisation use the following line instead of the datavis.animate line
    # for i in range(2000+1): training_step(i, i % 50 == 0, i % 10 == 0)

    #print("max test accuracy: " )#+ str(datavis.get_max_test_accuracy()))

    # final max test accuracy = 0.9268 (10K iterations). Accuracy should peak above 0.92 in the first 2000 iterations.

if __name__ == '__main__':
    main()