function name_array = get_names(path,windows)
%Creates a cell array of file names in the current directory.
cd(path);
filenames = ls;

%create the array of names taking into account the operating system
name_array = string([]);
if windows
    %remove . and ..
    while strcmp(filenames(1),'.')
        filenames(1,:) = [];
    end
    %strip whitespace
    for i=1:length(filenames)
        name_array = [name_array; strtrim(filenames(i,:))];
    end
else
    name_array = strsplit(strip(filenames));
end