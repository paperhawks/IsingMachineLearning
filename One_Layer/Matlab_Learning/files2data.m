function [full_lattice, full_vars] = files2data(path, name_array, windows)

if exist('full_lattice','var') == 0 || exist('full_vars','var') == 0
    full_lattice = [];
    full_vars = [];
    for i = 1:length(name_array)
        filename = "";
        if windows
            filename = strcat(path, name_array(i,:));
        else
            filename = cell2mat(strcat(path, name_array(i)));
        end
        flat_data = import_data(filename);
        [lattice, system_vars] = parse_data(flat_data);

        %concatonate
        full_lattice = [full_lattice; lattice'];
        full_vars = [full_vars; system_vars];
    end
end