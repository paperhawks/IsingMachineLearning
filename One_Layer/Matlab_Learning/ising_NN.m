%Tries to predict the Ising model's phase transition point in learning from a neural network.
%% Get files 
windows = false; %flag for Windows (they handle paths differently)
path = '../N10/';
name_array = get_names(path, windows);

%go back to Matlab_Learning to import data
cd ../Matlab_Learning/;

%% Split the data into the training and 'testing' sets
% We want to see if we can find the phase transition point, which is
% 2J/(k(ln(1+sqrt(2))) ~= 2.269. Points 'near' there are seperated as 
% an 'apply' set. 
%apply_names = {'T2.15385.N10', 'T2.21795.N10', 'T2.28205.N10', 'T2.34615.N10'};
%name_array = setdiff(name_array, apply_names);

%% Create data from the files
[full_lattice, full_vars] = files2data(path, name_array,windows);
%[trans_lattice, trans_vars] = files2data(path, apply_names, windows);
train_vars = full_vars(:,[4,5]);
%test_vars = trans_vars(:,[4,5]);

%% Train and test the neural network
net = fitnet(1, 'traincgp');
[net,tr] = train(net, full_lattice', train_vars');
%net_vars = net(trans_lattice');
%e = test_vars' - net_vars;

%% Post Processing Analysis
err = immse(net_vars(1,:)',test_vars(:,1)); %Mean squared error
mean_diff = mean(net_vars(1,:)) - mean(test_vars(:,1)); %difference of the means
