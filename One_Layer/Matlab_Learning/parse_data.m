function [slice, system_vars] = parse_data(flat_data)
%Parses the data into the lattice and the variables that describe the
%system for the given number of variables.
%TODO: Need to generalize to bigger systems and general number of samples.

lattice = zeros(length(flat_data(1,:)),length(flat_data(1,:)));
system_vars = zeros(length(flat_data(1,:)),7);

counter_latt = 1;
counter_sys = 1;
for i = 1:length(flat_data)
    
    %System variables data.
    if mod(i,11) == 0
        system_vars(counter_sys,:) = flat_data(i,1:7);
        counter_sys = counter_sys + 1;
    %Lattice data.
    else
        lattice(counter_latt,:) = flat_data(i,:);
        counter_latt = counter_latt + 1;
    end
end

%Reshape the lattice into one dimensional array.
for i = 1:1000
   slice(:,i) = reshape(lattice((i-1)*10+1:(i-1)*10 + 10,:),[100,1])'; 
end