from scipy import *
from pylab import *
from scipy import random
from scipy import weave

#Build lattice
def RandomL(N):
	""" Generates random lattice for Ising simulation."""
	latt = sign(2*random.random((N,N))-1.) 
	latt = array(latt, dtype = int)
	return latt

def CEnergy(latt):
	"Computes energy of 2D Ising configuration."
	Ene = 0.
	N = len(latt)

	#loop through entire lattice
	for i in range(N):
		for j in range(N):
			S = latt[i,j]

			#sum over nearest neighbors
			WF = latt[(i+1)%N, j] + latt[(i-1)%N, j] + latt[i, (j+1)%N] + latt[i, (j-1)%N]
			Ene -= WF * S 
	
	return Ene/2.	

def SampleCPP(T, Nitt, latt, PW, warm, measure):
	Ene = float(CEnergy(latt)) #starting Energy
	Mn = float(sum(latt))		#starting magnetization
	N = int(len(latt))

	aver = zeros(5, dtype = float)

	code = """
	#line 37 "ising.py"
	srand48(time(0));
	
	using namespace std;

	int N2 = N*N;
	for (int itt = 0; itt < Nitt; itt++)
	{
		int t = static_cast<int>(drand48()*N2);
		int i = t%N;
		int j = t/N;
		int S = latt(i,j);

		int WF = latt((i+1)%N, j) + latt((i-1+N)%N, j) + latt(i, (j+1)%N) + latt(i, (j-1+N)%N);

		//energy change 2*J*WF*S 
		double P = PW(4+S*WF);

		if (P > drand48()) //flip the spin
		{
			latt(i,j) = -S;
			Ene += 2*S*WF;
			Mn -= 2*S ;
		}

		//calculate values 
		if (itt > warm && itt % measure == 0)
		{
			aver(0) += 1;
			aver(1) += Ene;
			aver(2) += Mn;
			aver(3) += Ene*Ene;
			aver(4) += Mn*Mn;
		}
	}

	"""
	weave.inline(code, ['Nitt', 'latt', 'N', 'PW', 'Ene', 'Mn', 'warm', 'measure', 'aver'],
		type_converters = weave.converters.blitz, compiler = 'gcc')

	E = aver[1]/aver[0]
	M = aver[2]/aver[0]
	cv = (aver[3]/aver[0] - (aver[1]/aver[0])**2 )/T**2
	chi = (aver[4]/aver[0] - (aver[2]/aver[0])**2)/T

	return M,E , cv, chi


def SamplePython(Nitt, latt, PW, warm, measure):
	"Monte carlo sampling"
	Ene = CEnergy(latt) #starting Energy
	Mn = sum(latt)		#starting magnetization
	N = len(latt)

	Eaver = 0.
	Maver = 0.
	Naver = 0.
	
	N2 = N*N 
	for itt in range(Nitt):
		t = int(rand()*N2)
		(i,j) = (t%N, t/N)
		S = latt[i,j]

		WF = latt[(i+1)%N, j] + latt[(i-1)%N, j] + latt[i, (j+1)%N] + latt[i, (j-1)%N]
		#energy change 2*J*WF*S 
		P = PW[4+S*WF]

		if P > rand(): #flip the spin
			latt[i,j] = -S
			Ene += 2*S*WF
			Mn -= 2*S 

		#calculate values 
		if itt > warm and itt % measure == 0:
			Eaver += Ene
			Maver += Mn
			Naver += 1

	# print Ene, Mn
	# print 'should be ', CEnergy(latt), sum(latt)
	return Maver/Naver, Eaver/Naver


def main():	
	N = 10
	latt = RandomL(N)
	Nitt = 50000000
	warm = 1000
	measure = 5

	wT = linspace(4, 0.25, 30)
	wMag = []
	wEne = []
	wcv = []
	wchi = []

	for T in wT:
		#pre-compute exponentials
		PW = zeros(9, dtype = float)
		PW[4+4] = exp(-4.*2/T)
		PW[2+4] = exp(-2.*2/T)
		PW[0+4] = 1
		PW[4-2] = exp( 2.*2/T)
		PW[4-4] = exp (4.*2/T)

		#(Mag, Ene) = SamplePython(Nitt, latt, PW, warm, measure)
		(Mag, Ene, cv, chi) = SampleCPP(T, Nitt, latt, PW, warm, measure)
		print "Mag = ", Mag, "Energy = ", Ene

		wMag.append( Mag/(N*N) )
		wEne.append( Ene/(N*N) )
		wcv.append( cv/(N*N) )
		wchi.append( chi/(N*N) )

	subplot(2,2,1)
	title('Magnetization')
	plot(wT, wMag)
	subplot(2,2,2)
	title('Energy')
	plot(wT, wEne)
	subplot(2,2,3)
	title('Specific heat')
	plot(wT, wcv)
	subplot(2,2,4)
	title('Susceptibility')
	plot(wT, wchi)
	show()

if __name__ == '__main__':
	main()
	# latt = RandomL(10)
	# Nitt = 100

	# T = 3

	# PW = zeros(9, dtype = float)
	# PW[4+4] = exp(-4.*2/T)
	# PW[2+4] = exp(-2.*2/T)
	# PW[0+4] = 1
	# PW[4-2] = exp( 2.*2/T)
	# PW[4-4] = exp (4.*2/T)

	# SamplePython(Nitt, latt, PW)