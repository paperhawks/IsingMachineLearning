#standard imports
import numpy as np 
import matplotlib.pyplot as plt 
from numpy.random import rand
from scipy import *
from scipy import weave

class Ising:
    """
    Defines the Ising model, including simulaations and 
    calculating all properties of the Ising model.
    """
    def __init__(self, N, T, J):
        self.N = N
        self.J = J
        self.lattice = self.RandomL(N)

    def RandomL(self, N):
            """ Generates random lattice for Ising simulation."""
            lattice = sign(2*random.random((N,N))-1.) 
            lattice = array(lattice, dtype = int)
            return lattice

    def Show_Lattice(self):
        print self.lattice

    def Get_Lattice(self):
        return self.lattice

    def Calc_Energy(self):
        """
        Calculates energy by direct calculation. 
        H = J * sum<i,j> s_i * s_j. 
        When fixing site i, we can rewrite this as 
        J * s_[p,q] (s_[p+1,q] + s_[p-1,q] + s_[p,q-1] + s_[p,q+1])
        """
        energy = 0
        N = len(self.lattice)
        for i in range(N):
            for j in range(N):
                spin = self.lattice[i][j]

                #sum over nearest neighbors
                nearest_neighbors = self.lattice[(i+1)%N, j] + self.lattice[(i-1)%N, j] + self.lattice[i, (j+1)%N] + self.lattice[i, (j-1)%N]
                energy -= nearest_neighbors * spin * self.J
    
        return energy/2. 

    def Correlation(self):
        """
        Creates the pair correlation function by creating a 
        histogram. 
        """
        corr = 0.
        #first point
        for i in range(self.N):
            for j in range(self.N):

                #second point
                for m in range(self.N):
                    for n in range(self.N):
                        corr += self.lattice[i,j] * self.lattice[m,n]

        return corr/self.N**2

    def SampleCPP(self, T, mc_steps = 10000, warmup = 1000, measurement_freq = 100):
        Ene = float(self.Calc_Energy()) #starting Energy
        Mn = float(sum(self.lattice))       #starting magnetization
        N = int(len(self.lattice))

        #pre-compute exponentials
        PW = zeros(9, dtype = float)
        PW[4+4] = exp(-4.*2/T)
        PW[2+4] = exp(-2.*2/T)
        PW[0+4] = 1
        PW[4-2] = exp( 2.*2/T)
        PW[4-4] = exp (4.*2/T)

        aver = zeros(5, dtype = float)

        code = """
        #line 37 "ising.py"
        srand48(time(0));
        
        using namespace std;

        int N2 = N*N;
        for (int itt = 0; itt < mc_steps; itt++)
        {
            int t = static_cast<int>(drand48()*N2);
            int i = t%N;
            int j = t/N;
            int S = self.lattice(i,j);

            int WF = self.lattice((i+1)%N, j) + self.lattice((i-1+N)%N, j) + self.lattice(i, (j+1)%N) + self.lattice(i, (j-1+N)%N);

            //energy change 2*J*WF*S 
            double P = PW(4+S*WF);

            if (P > drand48()) //flip the spin
            {
                self.lattice(i,j) = -S;
                Ene += 2*S*WF;
                Mn -= 2*S ;
            }

            //calculate values 
            if (itt > warm && itt % measure == 0)
            {
                aver(0) += 1;
                aver(1) += Ene;
                aver(2) += Mn;
                aver(3) += Ene*Ene;
                aver(4) += Mn*Mn;
            }
        }

        """
        weave.inline(code, ['Nitt', 'latt', 'N', 'PW', 'Ene', 'Mn', 'warm', 'measure', 'aver'],
            type_converters = weave.converters.blitz, compiler = 'gcc')

        E = aver[1]/aver[0]
        M = aver[2]/aver[0]
        cv = (aver[3]/aver[0] - (aver[1]/aver[0])**2 )/T**2
        chi = (aver[4]/aver[0] - (aver[2]/aver[0])**2)/T

        return M,E , cv, chi

    def Equilibrate(self, T, mc_steps = 10000, warmup = 1000, measurement_freq = 100):
        """
        Monte carlo sampling. Equalibrates the system using mc_steps Monte
        Carlo steps. 
        """
        energy = self.Calc_Energy() #starting energy
        Mn = sum(self.lattice)      #starting magnetization
        N = self.N                  #size of lattice

        #pre-compute exponentials
        PW = zeros(9, dtype = float)
        PW[4+4] = exp(-4.*2/T)
        PW[2+4] = exp(-2.*2/T)
        PW[0+4] = 1
        PW[4-2] = exp( 2.*2/T)
        PW[4-4] = exp (4.*2/T)

        print PW

        E_tot = 0.
        M_tot = 0.
        N_tot = 0.
        E_sq_tot = 0.
        M_sq_tot = 0.
        
        N2 = N*N 
        for itt in range(mc_steps):
            t = int(rand()*N2)
            (i,j) = (t%N, t/N)
            S = self.lattice[i,j]

            WF = self.lattice[(i+1)%N, j] + self.lattice[(i-1)%N, j] + self.lattice[i, (j+1)%N] + self.lattice[i, (j-1)%N]
            WF = WF*self.J

            #energy change 2*J*WF*S 
            P = PW[4+S*WF]

            if P > rand(): #flip the spin
                self.lattice[i,j] = -S
                energy += 2*S*WF*self.J
                Mn -= 2*S 

            #calculate values 
            if itt > warmup and warmup % measurement_freq == 0:
                E_tot += energy
                E_sq_tot += energy**2

                M_tot += Mn
                M_sq_tot += Mn**2
                N_tot += 1
        print N_tot
        #calculate expectation values
        E = E_tot/N_tot/N2
        M = M_tot/N_tot
        cv = (E_sq_tot/N_tot - (E_tot/N_tot)**2 )/T**2
        chi = (M_sq_tot/N_tot - (M_tot/N_tot)**2)/T
        return M, E, cv, chi

def main():
    #physical parameters
    N = 15              #size of the Ising model (assuming square lattice)
    T = 1.0             #Temperature 
    J = 1               #Coupling constant between nearest neighbor spins

    #Computational parameters
    mc_steps = 10000 
    warmup = 5000 
    measurement_freq = 200

    ising = Ising(N, T, J)
    ising.Show_Lattice()
    M, E, cv, chi = ising.Equilibrate(T, mc_steps, warmup, measurement_freq)
    print "Magnetization:", M
    print "Energy:", E
    print "Specific heat:", cv
    print "Susceptibility:", chi
    ising.Show_Lattice()

if __name__ == '__main__':
    main()
