#standard imports
import numpy as np 
import matplotlib.pyplot as plt 
from numpy.random import rand
from scipy import *

class Ising:
    """
    Defines the Ising model, including simulaations and 
    calculating all properties of the Ising model.
    """
    def __init__(self, N, T, J):
        self.N = N
        self.J = J
        self.lattice = self.RandomL(N)

    def RandomL(self, N):
            """ Generates random lattice for Ising simulation."""
            lattice = sign(2*random.random((N,N))-1.) 
            lattice = array(lattice, dtype = int)
            return lattice

    def Show_Lattice(self):
        print self.lattice

    def Calc_Energy(self):
        """
        Calculates energy by direct calculation. 
        H = J * sum<i,j> s_i * s_j. 
        When fixing site i, we can rewrite this as 
        J * s_[p,q] (s_[p+1,q] + s_[p-1,q] + s_[p,q-1] + s_[p,q+1])
        """
        energy = 0
        N = len(self.lattice)
        for i in range(N):
            for j in range(N):
                spin = self.lattice[i][j]

                #sum over nearest neighbors
                nearest_neighbors = self.lattice[(i+1)%N, j] + self.lattice[(i-1)%N, j] + self.lattice[i, (j+1)%N] + self.lattice[i, (j-1)%N]
                energy -= nearest_neighbors * spin * self.J
    
        return energy/2. 


    def Equilibrate(self, mc_steps, T, warmup, measurement_freq):
        """
        Monte carlo sampling. Equalibrates the system using mc_steps Monte
        Carlo steps. 
        """
        energy = self.Calc_Energy() #starting energy
        Mn = sum(self.lattice)      #starting magnetization
        N = self.N                  #size of lattice

        #pre-compute exponentials
        PW = zeros(9, dtype = float)
        PW[4+4] = exp(-4.*2/T)
        PW[2+4] = exp(-2.*2/T)
        PW[0+4] = 1
        PW[4-2] = exp( 2.*2/T)
        PW[4-4] = exp (4.*2/T)

        E_tot = 0.
        M_tot = 0.
        N_tot = 0.
        E_sq_tot = 0.
        M_sq_tot = 0.
        
        N2 = N*N 
        for itt in range(mc_steps):
            t = int(rand()*N2)
            (i,j) = (t%N, t/N)
            S = self.lattice[i,j]

            WF = self.lattice[(i+1)%N, j] + self.lattice[(i-1)%N, j] + self.lattice[i, (j+1)%N] + self.lattice[i, (j-1)%N]
            WF = WF*self.J

            #energy change 2*J*WF*S 
            P = PW[4+S*WF]

            if P > rand(): #flip the spin
                self.lattice[i,j] = -S
                energy += 2*S*WF*self.J
                Mn -= 2*S 

            #calculate values 
            if itt > warmup and warmup % measurement_freq == 0:
                E_tot += energy
                E_sq_tot += energy**2

                M_tot += Mn
                M_sq_tot += Mn**2
                N_tot += 1

        #calculate expectation values
        E = E_tot/N_tot/N2
        M = M_tot/N_tot
        cv = (E_sq_tot/N_tot - (E_tot/N_tot)**2 )/T**2
        chi = (M_sq_tot/N_tot - (M_tot/N_tot)**2)/T
        return M, E, cv, chi

def main():
    #physical parameters
    N = 10              #size of the Ising model (assuming square lattice)
    T = 2.0             #Temperature 
    J = 1               #Coupling constant between nearest neighbor spins

    #Computational parameters
    mc_steps = 10000 
    warmup = 5000 
    measurement_freq = 1000

    ising = Ising(N, T, J)
    ising.Show_Lattice()
    M, E, cv, chi = ising.Equilibrate(mc_steps, T, warmup, measurement_freq)
    print "Magnetization:", M
    print "Energy:", E
    print "Specific heat:", cv
    print "Susceptibility:", chi
    ising.Show_Lattice()

if __name__ == '__main__':
    main()