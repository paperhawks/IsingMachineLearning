import ising as ism 
import numpy as np 

def main():
    #numerical parameters
    n_configs = 1 

    #physical parameters
    T = np.linspace(1,10, 100) # list of temperatures to generate configs
    J = 1
    N = 10

    configs = []
    #generate configurations for specified temperature
    for i in range(n_configs):
        model = ism.Ising(N, T[0], J)
        M, E, cv, chi = model.SampleCPP(T[0])
        print "Magnetization:", M
        print "Energy:", E
        print "Specific heat:", cv
        print "Susceptibility:", chi
        model.Show_Lattice()

        #print "Correlation:", model.Correlation()
        model.Show_Lattice()
        configs.append(model.Get_Lattice())


if __name__ == '__main__':
    main()