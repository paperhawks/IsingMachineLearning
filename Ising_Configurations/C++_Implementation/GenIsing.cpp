//standard includes
#include<vector>
#include<iostream>
#include<fstream> //file operations
#include<sstream>
#include<string>
#include<time.h> // for random

//math
#include<stdlib.h>
#include<math.h>

//Our Ising model implementation
#include"model_ising.h"

using namespace std;

void store_system(Ising model, const char * name, double energy, double mag, double cv, double chi, int N, double T, double J)
{
	//open a file to write
	ofstream ising_database;
	ising_database.open(name, ios_base::app);

	//write lattice
	vector< vector<int> > lattice = model.get_lattice();
	for(size_t i = 0; i < lattice.size(); ++i)
    {   
        for(size_t j = 0; j < lattice.size(); ++j)
        {
            if(lattice[i][j] == 1)
                ising_database << "  " << lattice[i][j];
            if(lattice[i][j] == -1) 
                ising_database << " " << lattice[i][j];
        }
        ising_database << endl;
    }
	ising_database << N << "," << T << "," << J << "," << energy << "," << mag << "," << cv << "," << chi << endl;
	ising_database.close();
}

string create_name(double T, int N)
{
	//create T_string 
	ostringstream T_stream;
	T_stream << T;
	string T_string = T_stream.str();

	//create N string
	ostringstream N_stream;
	N_stream << N;
	string N_string = N_stream.str();

	return "T" + T_string + ".N" + N_string;

}

//linspace for doubles
vector<double> linspace(double minT, double maxT, int T_points)
{
	vector<double> T_vec;
	for(int i = 0; i < T_points; ++i)
		T_vec.push_back((maxT-minT)/(T_points-1)*i + minT);

	return T_vec;
}

//linspace for ints
vector<int> linspace_int(int minN, int maxN, int N_points)
{
	//create N-size vector
	vector<int> N_vec;

	for(int i = 0; i < N_points; ++i)
		N_vec.push_back((maxN - minN)/(N_points-1)*i + minN);

	return N_vec;
}

int main()
{
	//physical parameters
	double J = 1.0;		//Coupling constant between nearest neighbor spins
	vector<double> T_vec = linspace(1.0, 3.5, 40);
	vector<int> N_vec = linspace_int(10, 50, 5);

	//computational paramters
	srand(time(NULL));
	int mc_steps = 10000;
	int warmup = 5000;
	int measurement_freq = 1000;
	int Nimgs = 1000; //number of images of the Ising model to generate

	//calcualte for all temperatures
	for(size_t i = 0; i < T_vec.size(); ++i)
	{
		//calculate for all sizes
		for(size_t j = 0; j < N_vec.size(); ++j)
		{
			//generate Nimgs number of images 
			for(int k = 0; k < Nimgs; ++k)
			{
				//create local paramters at given temperature
				double T = T_vec[i];
				double N = N_vec[j];
	
				//equilibriate Ising model and store the results
				Ising model = Ising(N, T, J);
				vector<double> sys_properties = model.equilibrate(mc_steps, warmup, measurement_freq);
//				model.print_system(sys_properties);
				string name = create_name(T, N);
				store_system(model, name.c_str(), sys_properties[0], sys_properties[1], sys_properties[2], sys_properties[3], N, T, J);
			}
		}
	}
	return 0;
}
