//standard includes
#include <vector>
#include <iostream>
#include <string>
#include <map>
#include <time.h>

//math
#include <stdlib.h>
#include <math.h>

//header
#include "model_ising.h"

using namespace std;
	
/**************CONSTRUCTORS************/
//default constructor, creates a uniform random Ising 
Ising::Ising()
{
	lattice = generate_lattice(10);
	N = 10;
	T = 1.;
	J = 1.;
}

//Constructor with arguement for size of Ising
Ising::Ising(int size)
{
	lattice = generate_lattice(size);
	N = size;
	T = 1.;
	J = 1.;
}

//Generates full Ising model specifying T, size, and J
Ising::Ising(int size, double temp, double coupling)
{
	lattice = generate_lattice(size);
	N = size;
	T = temp;
	J = coupling;
//	cout << "Ising model generated with size " << size << " at temperature " << T << 
//			" coupling strength " << J << "." << endl;
}

/************GETTERS***********/
int Ising::get_size()
{
	return N;
}

//getter for the coupling constant
double Ising::get_J()
{
	return J;
}

//getter for the lattice
vector< vector<int> > Ising::get_lattice()
{
	return lattice;
}

//displays the lattice in std-out
void Ising::show_lattice()
{
	for(size_t i = 0; i < lattice.size(); ++i)
	{
		for(size_t j = 0; j < lattice.size(); ++j)
		{
			if(lattice[i][j] == 1)
				cout << " " << lattice[i][j];
			if(lattice[i][j] == -1)
				cout << lattice[i][j];
		}
		cout << endl;
	}
}
/**********SETTERS***********/
//sets the size of the lattice
void Ising::set_size(int size)
{
	N = size;
}

//sets the coupling constant
void Ising::set_J(double coupling)
{
	J = coupling;
}

/*********ISING*******/
//generates a uniformly distrubuted lattice
vector< vector<int> > Ising::generate_lattice(int size)
{
	vector< vector<int> > latt;

	//populate the lattice 
	for( int i = 0; i < size; ++i)
	{
		vector<int> lattice_row;
		for(int j = 0; j < size; ++j)
		{
			int rand_val = (rand() % 2) -1;
			
			//spin is +/- 1
			if(rand_val == 0)
				rand_val = 1;
			lattice_row.push_back(rand_val);
		}
		latt.push_back(lattice_row);	
	}
	return latt;
}

//calculates the energy of a current configuration of the Ising model
double Ising::calc_energy()
{
	double energy = 0.;
	for(int i = 0; i < N; ++i)
	{
		for(int j = 0; j < N; ++j)
		{
			int spin = lattice[i][j];

			//precompute the sum of nearest neighbors by factoring out spin
			int nearest_neighbors = lattice[(i+1)%N][j] + lattice[(i-1+N)%N][j] + lattice[i][(j+1)%N] + lattice[i][(j-1+N)%N];
			energy -= nearest_neighbors * spin * J;
		}
	}
	
	return energy/2.;//2 accounts for double counting
}

//Calculates the magnetization of the Ising system
double Ising::magnetization()
{
	double mag = 0.;
	for(int i = 0; i < N; ++i)
		for(int j = 0; j < N; ++j)
			mag += lattice[i][j];

	return mag;
}

//calculates the distance between two points
/*
Assumes that the input is in the form (i1,i2,...,iN).
We are calculating the L_2 norm.
*/
double Ising::distance(vector<int> point1, vector<int> point2)
{	
	//error check
	if(point1.size() != point2.size())
	{
		cout << "Vector lengths do not match." << endl;
		return 0.;
	}

	double dv = 0.;
	for(int i = 0; i < 2; ++i)
		dv += pow((point2[i] - point1[i]),2);
		
	return pow(dv, .5);
}

//returns the lattice points within radius R from the center
vector< vector<int> > Ising::legal_points(double R)
{
	vector< vector<int> > list_of_points;
	vector<int> center(2);
	center[0] = N/2;
	center[1] = N/2;

	for(int i = 0; i < N; ++i)
	{
		for(int j = 0; j < N; ++j)
		{
			//compare point distance from N/2
			vector<int> point(2);
			point[0] = i;
			point[1] = j;
			
			if(distance(point, center) <= R)
			{
				//cout << "R: " << R << "(" << i << "," << j << ")" << endl;
				list_of_points.push_back(point);
			}
		}
	}

	return list_of_points;			
}

//calculates the correlation with center points
double Ising::correlate(vector< vector<int> > legal_points)
{
	double corr = 0.;

	//calculate for all correlation
	for(size_t i = 0; i < legal_points.size(); ++i)
	{	
		//don't want to correlate with ourselves
		if(legal_points[i][0] != N/2 && legal_points[i][1] != N/2)
		{
			int x = legal_points[i][0];
			int y = legal_points[i][1];
			corr += lattice[x][y] * lattice[N/2][N/2];
			//cout << "mult: " << lattice[x][y] << "*" << lattice[N/2][N/2] << "=" << lattice[x][y] * lattice[N/2][N/2] << endl;
		}
	}
	//cout << "corr: " << corr << "size: " << legal_points.size() << endl;
	return corr/(legal_points.size()-1);
}

/*Calculates the correlation function of the Ising system
from the center to a maximum radius of the size of the box*/
pair<vector<double>, vector<double> > Ising::correlation()
{
	//create radius vector 
	vector<double> radii;
	vector<double> corr;
	for(int i = 1; i < N/2; ++i)
	{
		radii.push_back(i);

		//calculate correlation at each distance
		vector< vector<int> > legalPoints = legal_points(i);
		corr.push_back(correlate(legalPoints));
	}
	
	pair<vector<double>, vector<double> > corr_func;
	corr_func.first = radii;
	corr_func.second = corr;
	return corr_func;
}

//Equilibrates the system but also serves as a MC Sampling algorithm
/*
INPUTS:
-------
1.)int mc_steps - # of Monte Carlo steps
2.)int warmup - # of burn in/warmup steps 
3.)int measurement_freq - # how often the physical parameters are sampled

OUTPUTS:
--------
1.) M - Magnetization of the Ising system
2.) E - Energy of the Ising system
3.) cv - specific heat of the Ising system
4.) chi - susceptibility of the Ising system
*/
vector<double> Ising::equilibrate(int mc_steps, int warmup, int measurement_freq)
{
	//create return values
	vector<double> sys_properties;
	double energy = calc_energy();
	double Mn = magnetization();
	sys_properties.push_back(energy);
	sys_properties.push_back(Mn);
	//cout << "Initial magnetization: " << sys_properties[1] << endl;
	sys_properties.push_back(0.);
	sys_properties.push_back(0.);

	//precompute the exponentials 
	vector<double> PW(9,0);
	for(int i = -4; i < 5; i += 2)
		PW[i + 4] =  exp(-i*J*2/T);

	//create intermediate total values
	double E_tot = 0., M_tot = 0., N_tot = 0., E_sq_tot = 0., M_sq_tot = 0.;
	
	//Run MCMC
	for(int step = 0; step < mc_steps; ++step)
	{
		//generate a value of R^2->R mapping
		int rand_val = (rand() % (N*N));
		int i = rand_val%N;
		int j = rand_val/N;
		int spin = lattice[i][j];

		//calculate boltzmann weight using pre-computed exponentials
		int nearest_neighbors = lattice[(i+1)%N][j] + lattice[(i-1+N)%N][j] + lattice[i]    [(j+1)%N] + lattice[i][(j-1+N)%N];
		//double weight = nearest_neighbors * J;
		double prob = PW[4+spin*nearest_neighbors];
		
		//flip the spin
		if(prob > ((double) rand()/(RAND_MAX)))
		{
			lattice[i][j] = -spin;
			energy += 2*spin*nearest_neighbors*J;
			Mn -= 2*spin;
		}

		//calculate values
		if(step > warmup && warmup % measurement_freq == 0)
		{
			E_tot += energy;
			E_sq_tot += energy * energy;
			
			M_tot += Mn;
			M_sq_tot += Mn*Mn;

			
			N_tot += 1;
		}
	}
//	cout << "N_tot " << N_tot << endl;
	//calculate expectation values
	sys_properties[0] = E_tot/N_tot/(N*N);
	sys_properties[1] = M_tot/N_tot;
	sys_properties[2] = (E_sq_tot/N_tot - (E_tot/N_tot)*(E_tot/N_tot))/(T*T);
	sys_properties[3] = (M_sq_tot/N_tot - (M_tot/N_tot)*(M_tot/N_tot))/T;
//	cout << "Final magnetization: " << magnetization() << endl;

	return sys_properties;
}

//Prints the lattice and system properites
void Ising::print_system(vector<double> sys_properties)
{
	show_lattice();
	print_system_properties(sys_properties);
}

//Prints the energy, magnetization, specific heat, and suscrptibility
void Ising::print_system_properties(vector<double> sys_properties)
{
	cout << "Energy: " << sys_properties[0] << endl;
	cout << "Magnetization: " << sys_properties[1] << endl;
	cout << "Specific heart: " << sys_properties[2] << endl;
	cout << "Susceptibility: " << sys_properties[3] << endl;
}

/*int main()
{
	//physical parameters
	int N = 15; 		 //size of Ising model (assuming square lattice)
	double J = 1.0; 	 //Coupling constant between nearest neighbor spins
	double T = 1.0;		 //temperature 

	//computational paramters
	srand(time(NULL));
	int mc_steps = 10000;
	int warmup = 5000;
	int measurement_freq = 100;

	Ising model = Ising(N, T, J);
	vector<double> sys_properties = model.equilibrate(mc_steps, warmup, measurement_freq);
	model.print_system(sys_properties);*/
	/*pair< vector<double>, vector<double> > corr_func = model.correlation();
	for(int i = 0; i < corr_func.first.size(); ++i)
		cout << corr_func.first[i] << " " << corr_func.second[i] << endl;*/
//	return 0;
//}
