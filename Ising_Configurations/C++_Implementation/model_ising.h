#ifndef ISING_H
#define ISING_H

#include <iostream>
#include <vector>
#include <map>
#include <string>

using namespace std;

class Ising
{
	public:
		//Constructors
		Ising();
		Ising(int size);
		Ising(int size, double temp, double coupling);		

		//Getter functions
		int get_size();
		double get_J();
		vector< vector<int> > get_lattice();
		void show_lattice();

		//Setter functions
		void set_size(int size);
		void set_J(double coupling);
		
		//generates the random lattice 
		vector< vector<int> > generate_lattice(int size);

		//prints the system properties and ising configuration
		void print_system(vector<double> sys_properties);
	
		//prints physical properties such as energy and magnetization
		void print_system_properties(vector<double> sys_properties);
		

		//returns the energy configuration of the lattice
		double calc_energy();
	
		//calculates the magnetization of the sytem
		double magnetization();

		//calculates the correlation function for a given lattice
		pair<vector<double>, vector<double> > correlation();

		//equilibrates the Ising system 
		vector<double> equilibrate(int mc_steps, int warmup, int measurement_freq);
		
	private:
		//Ising model parameters
		int N; 						   //size of lattice
		double J; 					   //coupling constant of the Ising model
		double T;
		vector< vector<int> > lattice; //lattice containing the spins

		//Helper functions
		double distance(vector<int> point1, vector<int> point2);
		vector< vector<int> > legal_points(double R);
		double correlate(vector< vector<int> > legal_points);	
};

#endif
